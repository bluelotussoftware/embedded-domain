package com.bluelotussoftware.embedded;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.embeddable.GlassFishException;

public class App {

    public static void main(String[] args) {
        GlassFishController gfc;

        try {
            if (args.length == 0) {
                gfc = new GlassFishController("/Users/jyeary/Desktop/glassfish3/glassfish");
            } else {
                gfc = new GlassFishController(args[0]);
            }

            Thread t = new Thread(gfc);
            t.start();
            System.out.println("\n\nPress CTRL + C to stop GlassFish Server.\n\n");
        } catch (GlassFishException e) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
