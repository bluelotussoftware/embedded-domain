/*
 *  Copyright 2011 Blue Lotus Software, LLC.
 *  Copyright 2011 John Yeary <jyeary@bluelotussoftware.com>.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.embedded;

import java.io.File;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.embeddable.GlassFish.Status;
import org.glassfish.embeddable.*;

/**
 * GlassFish Controller for starting an embedded GlassFish
 * instance.
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
public class GlassFishController implements Runnable {

    private final String GLASSFISH_INSTALLROOT;
    private BootstrapProperties bootstrapProperties;
    private GlassFishRuntime glassFishRuntime;
    private GlassFishProperties glassFishProperties;
    private GlassFish glassFish;

    /**
     * Constructor. The constructor also initializes a shutdown hook.
     * @param warFilePathElements an array of path location elements.
     * @throws GlassFishException if an exception occurs during instance initialization.
     */
    public GlassFishController(String glassFishInstallationRoot) throws GlassFishException {
        if (glassFishInstallationRoot != null) {
            GLASSFISH_INSTALLROOT = glassFishInstallationRoot;
        } else {
            throw new GlassFishException("The installation root was null");
        }

        bootstrapProperties = new BootstrapProperties();
        Properties properties = bootstrapProperties.getProperties();
        properties.list(System.out);
        bootstrapProperties.setInstallRoot(GLASSFISH_INSTALLROOT);
        glassFishRuntime = GlassFishRuntime.bootstrap(bootstrapProperties);
        glassFishProperties = new GlassFishProperties();
        glassFishProperties.setInstanceRoot(GLASSFISH_INSTALLROOT + "/domains/domain1");
        System.out.println("GlassFish Instance Root: " + glassFishProperties.getInstanceRoot());
        glassFishProperties.setConfigFileURI(new File(GLASSFISH_INSTALLROOT + "/domains/domain1/config/domain.xml").toURI().toString());
        System.out.println("GlassFish Configuration: " + glassFishProperties.getConfigFileURI());
        glassFish = glassFishRuntime.newGlassFish(glassFishProperties);

        // add shutdown hook to stop server
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                shutdown();
            }
        });
    }

    /**
     * Runnable implementation which automatically deploys any war files supplied
     * in the constructor.
     */
    @Override
    public void run() {
        try {
            glassFish.start();
        } catch (GlassFishException ex) {
            Logger.getLogger(GlassFishController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This stops the GlassFish instance and un-deploys any war files.
     */
    public void stop() {
        try {
            glassFish.stop();
        } catch (GlassFishException ex) {
            Logger.getLogger(GlassFishController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This shuts down the GlassFish instance if running, and not disposed. This method is called
     * by the shutdown hook.
     */
    public void shutdown() {
        try {

            if (Status.STOPPED != glassFish.getStatus()) {
                stop();
            }

            if (Status.DISPOSED != glassFish.getStatus()) {
                glassFish.dispose();
            }

        } catch (GlassFishException ex) {
            Logger.getLogger(GlassFishController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Wrapper method which returns container deployer.
     * @return an instance of the GlassFish <code>Deployer</code> which is used
     * to deploy war and ear files.
     * @throws GlassFishException if the deployer can not be retrieved.
     */
    public Deployer getDeployer() throws GlassFishException {
        return glassFish.getDeployer();
    }
}
